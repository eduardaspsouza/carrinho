<%-- 
    Document   : carrinho
    Created on : 20/04/2015, 09:48:22
    Author     : Eduarda
--%>

<%@page import="com.br.livro.model.ItemDeCompra"%>
<%@page import="com.br.livro.model.CarrinhoDeCompra"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Carrinho de Compras!</h1>
        <table border="1" cellpadding="2" >
            <tr>
                <td bgcolor="#000088"><font color="white">Excluir</font></td>
                <td bgcolor="#000088"><font color="white">Item</font></td>
                <td bgcolor="#000088"><font color="white">QTD</font></td>
                <td bgcolor="#000088"><font color="white">Preço Unitário</font></td>
                <td bgcolor="#000088"><font color="white">Total Item</font></td>
                <td bgcolor="#000088"><font color="white">+1</font></td>
            </tr>
            <%
                //recupera os produtos do carrinho da sessao
                CarrinhoDeCompra carrinho = (CarrinhoDeCompra) session.getAttribute("carrinho");
                for (ItemDeCompra item : carrinho.getItens()) {
            %>
            <tr>
                <td><a
                        href="ControleCarrinho?acao=removeProduto&idProduto=<%=item.getProduto().getIdProduto()%>">
                        X</td>
                <td><%=item.getProduto().getNome()%></td>
                <td><%=item.getQuantidade()%></td>
                <td><%=item.getProduto().getPrecoUnitario()%></td>
                <td><%=item.getTotal()%></td>
                <td><a
                        href="ControleCarrinho?acao=addProduto&idProduto=<%=item.getProduto().getIdProduto()%>">+</a
                    ></td>
            </tr>

            <%
                }
            %>

        </table>

        <strong>Valor Total: <%=carrinho.calculaTotal()%></strong><br/>

        <a href="index.jsp"> Continue comprando</a><br/>

            <a href="ControleCarrinho?acao=cancelaCompra">Cancelar comprar</a>
    </body>
</html>
