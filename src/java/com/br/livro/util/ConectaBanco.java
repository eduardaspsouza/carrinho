/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.livro.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Eduarda
 */
public class ConectaBanco {

    public static Connection getConexao() {
        Connection conexao = null;
        try {
            //driver que será utilizado
            Class.forName("com.mysql.jdbc.Driver");
            //cria um objeto de conexao com um banco especificado no caminho...
        //    conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/produtos?zeroDateTimeBehavior=convertToNull [root em Esquema default]","root", "root");
            conexao = DriverManager.getConnection("jdbc:mysql://localhost:80/livro","", "");
        
        
        } catch (ClassNotFoundException erro1) {
            throw new RuntimeException(erro1);

        } catch (SQLException erro2) {
            throw new RuntimeException(erro2);

        }
        return conexao;
    }

}
