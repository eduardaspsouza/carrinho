/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.livro.model;

/**
 *
 * @author Eduarda
 */
public class ItemDeCompra {
    private  Integer id;
    private Produto produto;
    private int quantidade;
    private double total;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public double getTotal(){
        this.total = this.quantidade * this.produto.getPrecoUnitario();
        return total;
    }
    
}
